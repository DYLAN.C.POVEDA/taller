import { Component, DoCheck, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

export interface user {
  mail: string,
  password: string
}

@Component({
  selector: 'app-sigin',
  templateUrl: './sigin.component.html',
  styleUrls: ['./sigin.component.scss']
})
export class SiginComponent implements OnInit,DoCheck {
  constructor(private auth:AuthService,private router:Router) { }
  digited:user={
    mail:'',
    password:''
  }
  repeteadPass:string="";
  validaConstrasena:boolean=true;

  
  ngDoCheck(): void {
    this.repeteadPass==this.digited.password?this.validaConstrasena=true:this.validaConstrasena=false
  }
  ngOnInit(): void {
  }
  submit(e:any){
    e.preventDefault()
    if(this.validaConstrasena){
      this.auth.createUser(this.digited).subscribe(
        res=>{
          localStorage.setItem('Token',res._id)
          this.router.navigate(['/peliculas'])
        },
        err=>{
          console.log(err);
          
        }
      )
    }

  }
}
