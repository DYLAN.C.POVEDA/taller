import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class OmdbService {
  url: string = 'https://pokeapi.co/api/v2/';
  constructor(private http: HttpClient) {}

  listPokemons(page:number){
    var offset=page*20;
    return this.http.get<any>(this.url+'pokemon?offset='+page+'&limit=20')
  }
  searchTitle(pokemon: string) {
    return this.http.get(this.url+'pokemon/'+pokemon);
  }
}
