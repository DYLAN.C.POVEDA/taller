import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'


interface user {
  _id:string,
  mail: string,
  password: string }
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient) { }
  //https://crudcrud.com/
  apiId: string = 'b19cf9b524a5424181865b160200a153'
  url: string = 'https://crudcrud.com/api/' + this.apiId

  isLogin(){
    if(localStorage.getItem('Token')){
      return true;
    };
    return false;
  }

  getUsers(){
    var urlGet=this.url+'/users'
    return this.http.get<user[]>(urlGet)
  }

  createUser(user: any) {
    var body = {
      mail: user.mail,
      password: user.password
    }
    var urlCreate=this.url+'/users'
    return this.http.post<any>(urlCreate,body)
  }
}
