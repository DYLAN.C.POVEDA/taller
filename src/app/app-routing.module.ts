import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardGuard } from './auth-guard.guard';
import { LoginComponent } from './login/login.component';
import { PeliculasComponent } from './peliculas/peliculas.component';
import { SiginComponent } from './sigin/sigin.component';

const routes: Routes = [
  {path:'login',component:LoginComponent, },
  { path:'peliculas',component:PeliculasComponent,canActivate:[AuthGuardGuard] },
  { path:'signin',component:SiginComponent },
  { path:'**',component:LoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
