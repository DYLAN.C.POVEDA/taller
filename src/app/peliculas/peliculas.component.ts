import { AfterViewInit, Component, DoCheck, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { OmdbService } from '../services/omdb.service';

@Component({
  selector: 'app-peliculas',
  templateUrl: './peliculas.component.html',
  styleUrls: ['./peliculas.component.scss'],
})
export class PeliculasComponent implements OnInit, AfterViewInit {
  searchedPokemon: string = '';
  page: number = 0;
  pagesize:number=5;
  pokemonGet: any;
  pokemonList!: MatTableDataSource<[]>;
  pokemonQuantity = 0;
  displayedColumns: string[] = ['numero', 'pokemon', 'boton'];
  count=0;
  constructor(private omdb: OmdbService) {
    this.pokemonList = new MatTableDataSource(this.pokemonGet);
  }

  
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  ngOnInit(): void {
    this.omdb.listPokemons(this.page).subscribe(
      (res) => {
        this.pokemonGet = res.results;
        this.pokemonQuantity = res.count;
        this.pokemonList.data=this.pokemonGet
      },
      (err) => {
        console.log(err);
      }
    );
  }
  ngAfterViewInit() {
    this.pokemonList.paginator = this.paginator;
    
  }
  pageEvent(){
    var index=(this.paginator.pageIndex)+1;
    var size=(this.paginator.pageSize);
    var numero=index*size
    this.page=index-1;
    this.pagesize=size
    
    if((numero%20==0)&&(numero>this.count*20)){
      this.count++;
      
      this.omdb.listPokemons(numero).subscribe(
        res=>{
          this.pokemonGet=this.pokemonGet.concat(res.results)
          this.pokemonQuantity = res.count;
          this.pokemonList.data=(this.pokemonGet)
        },
        err=>{
          console.log(err);
          
        }
      )
      
    }
  }
  submit() {
    this.omdb.searchTitle(this.searchedPokemon).subscribe(
      (res) => {
        console.log(res);
      },
      (err) => {
        console.log(err);
      }
    );
  }
}
