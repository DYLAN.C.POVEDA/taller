import { Component, DoCheck, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { user } from '../sigin/sigin.component';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit,DoCheck {

  digited:user={
    mail:'',
    password:''
  }
  
  
  validado:boolean=false;
  submitted:boolean=false;
  
  constructor(private auth:AuthService,private router:Router) { }


  isLogged:boolean=false;
  ngDoCheck(): void {
    this.isLogged=this.auth.isLogin();
  }
  ngOnInit(): void {
  }
  submit(e: any) {
    
    e.preventDefault();
    this.auth.getUsers().subscribe(
      res=>{
        console.log(res);
          res.forEach(element => {
            (element.password==this.digited.password)&&(element.mail==this.digited.mail)
            ?(localStorage.setItem('Token',element._id),
            this.validado=true,
            this.router.navigate([ '/peliculas']))
            :this.submitted=true;
            ;
          });
      },
      err=>{
        console.log(err)
      }
    )
  }
}
