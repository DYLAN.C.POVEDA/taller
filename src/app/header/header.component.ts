import { Component, DoCheck, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit,DoCheck {

  constructor(private auth:AuthService, private router:Router) { }
  isLogged:boolean=false;
  ngDoCheck(): void {
    this.isLogged=this.auth.isLogin();
  }

  logOut(){
    localStorage.removeItem('Token')
    this.router.navigate(['/login'])
  }
  ngOnInit(): void {
  }

}
